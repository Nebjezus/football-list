//
//  FollowersResponse.swift
//  football-list
//
//  Created by Benjamin on 08/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation

struct FollowersResponse : Decodable {
    
    enum CodingKeys : String, CodingKey {
        case followers = "response"
    }
    
    let followers : [User]?
}
