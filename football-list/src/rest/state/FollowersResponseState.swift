//
//  FollowerResponse.swift
//  football-list
//
//  Created by Benjamin on 08/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation
import RxRestClient

struct FollowersResponseState : ResponseState {
    typealias Body = Data
    
    var state : BaseState?
    var data : FollowersResponse?
    
    init(state: BaseState) {  }
    
    init(response: (HTTPURLResponse, Data?)) {
        if response.0.statusCode == 200, let body = response.1 {
            do {
                self.data = try JSONDecoder().decode(FollowersResponse.self, from: body)
            } catch let parsingError {
                print("Parsing error: \(parsingError).")
            }
        }
    }
}
