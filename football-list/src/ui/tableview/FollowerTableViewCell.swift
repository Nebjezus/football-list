//
//  FollowerTableViewCell.swift
//  football-list
//
//  Created by Benjamin on 08/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation
import UIKit

import RxCocoa
import RxSwift

class FollowerTableViewCell : UITableViewCell {
    
    @IBOutlet var userView : SimpleUserView!
    
    public func setUser(_ user : User) {
        userView.setUser(user)
    }
}
