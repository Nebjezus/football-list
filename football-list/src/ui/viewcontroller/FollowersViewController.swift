//
//  FollowersViewController.swift
//  football-list
//
//  Created by Benjamin on 08/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import UIKit

import RxCocoa
import RxSwift

class FollowersViewController : MVVMViewController<FollowersViewModel>, UITableViewDelegate {
    
    private let idenfitierMain : String = "cell_main"
    private let cellHeight : CGFloat = 88
    
    @IBOutlet var tableView : UITableView!
    
    override func bindViewModel(_ viewModel : FollowersViewModel, _ disposeBag : DisposeBag) {
        super.bindViewModel(viewModel, disposeBag)
        
        viewModel
            .allFollowers
            .bind(to:tableView!.rx.items(cellIdentifier: idenfitierMain, cellType: FollowerTableViewCell.self)) { (index, user, cell) in
                cell.setUser(user)
            }.disposed(by: disposeBag)
        
        tableView.rx.prefetchRows
            .bind(onNext: viewModel.prefetchCells)
            .disposed(by: disposeBag)
        
        tableView.rx.modelSelected(User.self)
            .bind(onNext: viewModel.onUserSelected)
            .disposed(by: disposeBag)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
}
