//
//  FollowerDetailsViewController.swift
//  football-list
//
//  Created by Benjamin on 11/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation
import UIKit

import RxSwift

class FollowerDetailsViewController : MVVMViewController<FollowerDetailsViewModel> {
    
    @IBOutlet var userView : SimpleUserView!
    @IBOutlet var buttonBack : UIButton!
    
    @IBOutlet var labelPositionName: UILabel!
    @IBOutlet var labelPositionOrder: UILabel!
    @IBOutlet var labelPositionGroup: UILabel!
    @IBOutlet var labelTeamName: UILabel!
    @IBOutlet var labelTeamCount: UILabel!
    @IBOutlet var labelLeagueName: UILabel!
    @IBOutlet var labelSeasonStart: UILabel!
    @IBOutlet var labelSeasonEnd: UILabel!
    
     override func bindViewModel(_ viewModel : FollowerDetailsViewModel, _ disposeBag : DisposeBag) {
        super.bindViewModel(viewModel, disposeBag)
        
        viewModel
            .user
            .bind(onNext: userView.setUser)
            .disposed(by: disposeBag)
        
        buttonBack.rx.tap
            .bind(onNext: viewModel.dismiss)
            .disposed(by: disposeBag)
        
        viewModel
            .primaryPositionName
            .asDriver()
            .drive(labelPositionName.rx.text)
            .disposed(by: disposeBag)
        
        viewModel
            .primaryPositionOrder
            .asDriver()
            .drive(labelPositionOrder.rx.text)
            .disposed(by: disposeBag)
        
        viewModel
            .primaryPositionGroup
            .asDriver()
            .drive(labelPositionGroup.rx.text)
            .disposed(by: disposeBag)
        
        viewModel
            .teamName
            .asDriver()
            .drive(labelTeamName.rx.text)
            .disposed(by: disposeBag)
        
        viewModel
            .teamCount
            .asDriver()
            .drive(labelTeamCount.rx.text)
            .disposed(by: disposeBag)
        
        viewModel
            .leagueName
            .asDriver()
            .drive(labelLeagueName.rx.text)
            .disposed(by: disposeBag)
        
        viewModel
            .seasonStart
            .asDriver()
            .drive(labelSeasonStart.rx.text)
            .disposed(by: disposeBag)
        
        viewModel
            .seasonEnd
            .asDriver()
            .drive(labelSeasonEnd.rx.text)
            .disposed(by: disposeBag)
    }
}
