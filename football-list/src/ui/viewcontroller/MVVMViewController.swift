//
//  MVVMViewController.swift
//  football-list
//
//  Created by Benjamin on 11/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import UIKit

import RxSwift
import RxCocoa

class MVVMViewController<T : VCViewModel> : BaseViewController {
    
    private var segueRequests : [SegueRequest] = [SegueRequest]()
    private var viewModel : T {
        if viewModelInternal == nil {
            viewModelInternal = T() as VCViewModel
        }
        return viewModelInternal as! T
    }
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindViewModel(viewModel, disposeBag)
    }
    
    func bindViewModel(_ viewModel : T, _ disposeBag : DisposeBag) {
        viewModel
            .segueRequests
            .do(onNext: { [weak self] (request) in
                if let request = request {
                    self?.segueRequests.append(request)
                }
            })
            .map({ (request) -> String? in
                if let request = request {
                    return request.segueIdentifier
                }
                return nil
            })
            .bind(onNext: performSegue)
            .disposed(by: disposeBag)
        
        viewModel
            .dismissRequests
            .bind(onNext: dismissViewController)
            .disposed(by: disposeBag)
    }
    
    func performSegue(_ identifier: String?) {
        if let identifier = identifier {
            performSegue(withIdentifier: identifier, sender: self)
        }
    }
    
    func dismissViewController() {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let request = segueRequests.first(where: { return $0.segueIdentifier == segue.identifier }) {
            segueRequests = segueRequests.filter { $0.segueIdentifier != request.segueIdentifier }
            let vc =  segue.destination as! BaseViewController
            vc.viewModelInternal = request.viewModel
        }
    }
}
