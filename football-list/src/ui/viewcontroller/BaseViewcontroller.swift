//
//  BaseViewController.swift
//  football-list
//
//  Created by Benjamin on 12/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController : UIViewController {
    var viewModelInternal : BaseViewModel? = nil
}
