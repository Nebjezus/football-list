//
//  SegueRequest.swift
//  football-list
//
//  Created by Benjamin on 12/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation

struct SegueRequest {
    let segueIdentifier : String
    let viewModel : VCViewModel
    
    init(_ segueIdentifier : String, _ viewModel : VCViewModel) {
        self.segueIdentifier = segueIdentifier
        self.viewModel = viewModel
    }
}
