//
//  FollowerDetailsViewModel.swift
//  football-list
//
//  Created by Benjamin on 11/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa

class FollowerDetailsViewModel : VCViewModel {
    
    let user : BehaviorRelay<User?> = BehaviorRelay<User?>(value:nil)
    
    let primaryPositionName : BehaviorRelay<String?> = BehaviorRelay<String?>(value: nil)
    let primaryPositionOrder : BehaviorRelay<String?> = BehaviorRelay<String?>(value: nil)
    let primaryPositionGroup : BehaviorRelay<String?> = BehaviorRelay<String?>(value: nil)
    
    let teamName : BehaviorRelay<String?> = BehaviorRelay<String?>(value: nil)
    let teamCount : BehaviorRelay<String?> = BehaviorRelay<String?>(value: nil)
    
    let leagueName : BehaviorRelay<String?> = BehaviorRelay<String?>(value: nil)
    let seasonStart : BehaviorRelay<String?> = BehaviorRelay<String?>(value: nil)
    let seasonEnd : BehaviorRelay<String?> = BehaviorRelay<String?>(value: nil)
    
    required init() {
        super.init()
    }
    
    init(_ user : User) {
        self.user.accept(user)
        self.primaryPositionName.accept(user.primaryPosition?.name)
        
        if let order = user.primaryPosition?.order {
            self.primaryPositionOrder.accept(String(order))
        } else {
            self.primaryPositionOrder.accept(nil)
        }
        
        if let group = user.primaryPosition?.group {
            self.primaryPositionGroup.accept(String(group))
        } else {
            self.primaryPositionGroup.accept(nil)
        }
        
        self.teamName.accept(user.team?.displayName)
        
        if let playerCount = user.team?.playersCount {
            self.teamCount.accept(String(playerCount))
        } else {
            self.teamCount.accept(nil)
        }
        
        if let leagueName = user.team?.leagueName {
            self.leagueName.accept(leagueName)
        } else {
            self.leagueName.accept(nil)
        }
        
        if let seasonStart = user.team?.season?.start {
            self.seasonStart.accept(seasonStart)
        } else {
            self.seasonStart.accept(nil)
        }
        
        if let seasonEnd = user.team?.season?.end {
            self.seasonEnd.accept(seasonEnd)
        } else {
            self.seasonEnd.accept(nil)
        }
        
        super.init()
    }
}
