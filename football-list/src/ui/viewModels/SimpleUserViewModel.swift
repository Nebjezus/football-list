//
//  SimpleUserViewModel.swift
//  football-list
//
//  Created by Benjamin on 11/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import UIKit

import RxSwift
import RxCocoa
import RxNuke
import Nuke

class SimpleUserViewModel : BaseViewModel {
    let fullname : BehaviorRelay<String?> = BehaviorRelay<String?>(value:"")
    let clubname : BehaviorRelay<String?> = BehaviorRelay<String?>(value:"")
    let profileUrl : BehaviorRelay<String?> = BehaviorRelay<String?>(value:"")
    let clubLogoUrl : BehaviorRelay<String?> = BehaviorRelay<String?>(value:"")
    
    let profileImage : Observable<UIImage?>
    let clubLogoImage : Observable<UIImage?>
    
    required init() {
        profileImage = profileUrl.flatMap({ (url) -> Observable<UIImage?> in
            if let urlString = url, let url = URL(string: urlString) {
                return ImagePipeline.shared.rx.loadImage(with: url).map({ (response) -> UIImage? in
                    return response.image
                }).asObservable()
            }
            return Observable.empty()
        })
        
        clubLogoImage = clubLogoUrl.flatMap({ (url) -> Observable<UIImage?> in
            if let urlString = url, let url = URL(string: urlString) {
                return ImagePipeline.shared.rx.loadImage(with: url).map({ (response) -> UIImage? in
                    return response.image
                }).asObservable()
            }
            return Observable.empty()
        })
    }
    
    func setUser(_ user : User) {
        fullname.accept(user.fullname)
        profileUrl.accept(user.profilePicture)
        clubname.accept(user.club.name)
        clubLogoUrl.accept(user.club.logoUrl)
    }
}
