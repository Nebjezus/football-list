//
//  VCViewModel.swift
//  football-list
//
//  Created by Benjamin on 12/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa

class VCViewModel : BaseViewModel {
    let segueRequests : BehaviorRelay<SegueRequest?> = BehaviorRelay<SegueRequest?>(value: nil)
    let dismissRequests : BehaviorRelay<Void> = BehaviorRelay<Void>(value: Void())
    
    func dismiss() {
        dismissRequests.accept(())
    }
}
