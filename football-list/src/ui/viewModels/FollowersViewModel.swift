//
//  FollowersViewModel.swift
//  football-list
//
//  Created by Benjamin on 08/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa

class FollowersViewModel : VCViewModel {
    
    private let segueDetails : String = "segue_follower_detail"
    
    let loadTollerance : Int = 5 // how many cells before the end should we start loading the next page
    let requestThrottle : RxTimeInterval = 2 // how often should we allow requests
    private let service : FollowerService = FollowerService()
    
    let nextPage : BehaviorRelay<Any?> = BehaviorRelay<Any?>(value:nil)
    let userId : BehaviorRelay<String> = BehaviorRelay<String>(value:"christian-planck")
    let slug : BehaviorRelay<String?> = BehaviorRelay<String?>(value:nil)
    let loadIndex : BehaviorRelay<Int> = BehaviorRelay<Int>(value:0)
    
    let allFollowers : Observable<[User]>
    let scheduler = ConcurrentDispatchQueueScheduler(qos: .background)
    
    required init() {
        allFollowers = nextPage
            .observeOn(scheduler)
            .throttle(requestThrottle, scheduler: scheduler)
            .withLatestFrom(slug)
            .flatMap({ [weak service, userId] (slug) -> Observable<[User]> in
                return (service?.followersForUser(userId: userId.value, slug: slug))!
            })
            .scan([User](), accumulator: { (allFollowers : [User], newFollowers : [User]) -> [User] in
                return allFollowers + newFollowers
            })
            .do(onNext: { [weak slug, loadIndex, loadTollerance] (followers) in
                let nextSlug = followers.count == 0 ? nil : followers.last?.slug
                slug?.accept(nextSlug)
                loadIndex.accept(followers.count - loadTollerance)
            })
    }
    
    func prefetchCells(_ cells : [IndexPath]) {
        if let lastCell = cells.last?.row, lastCell >= loadIndex.value {
            nextPage.accept(1)
        }
    }
    
    func onUserSelected(_ follower : User) {
        let detailsVm : FollowerDetailsViewModel = FollowerDetailsViewModel(follower)
        let request : SegueRequest = SegueRequest(segueDetails,detailsVm)
        segueRequests.accept(request)
    }
}
