//
//  SimpleUserView.swift
//  football-list
//
//  Created by Benjamin on 11/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation
import UIKit

import RxSwift
import RxCocoa

class SimpleUserView : UIView {
    private let nibName : String = "SimpleUserView"
    private let viewModel : SimpleUserViewModel = SimpleUserViewModel()
    private let disposeBag = DisposeBag()
    private var imageDisposeBag = DisposeBag()
    
    @IBOutlet var contentView : UIView!
    @IBOutlet var labelName : UILabel!
    @IBOutlet var labelClub : UILabel!
    @IBOutlet var imageProfile : UIImageView!
    @IBOutlet var imageClubLogo : UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override func awakeFromNib() {
        bindViewModel()
    }
    
    private func setup() {
        Bundle.main.loadNibNamed(nibName, owner: self, options: nil)
        self.addSubview(self.contentView!)
        self.contentView.frame = self.frame
        self.contentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    public func setUser(_ user : User?) {
        imageDisposeBag = DisposeBag()
        
        if (user == nil) {
            return
        }
        
        viewModel
            .profileImage
            .bind(to: imageProfile.rx.image)
            .disposed(by: imageDisposeBag)
        
        viewModel
            .clubLogoImage
            .bind(to: imageClubLogo.rx.image)
            .disposed(by: imageDisposeBag)
        
        viewModel.setUser(user!)
    }
    
    private func bindViewModel() {
        viewModel
            .fullname
            .asDriver()
            .drive(labelName.rx.text)
            .disposed(by: disposeBag)
        
        viewModel
            .clubname
            .asDriver()
            .drive(labelClub.rx.text)
            .disposed(by: disposeBag)
    }
}
