//
//  BaseService.swift
//  football-list
//
//  Created by Benjamin on 08/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation

import RxSwift
import RxRestClient

class BaseService {
    static let baseUrl : String = "http://api.tonsser.com"
    static let endpointVersion : Int = 49
    
    private static let client : RxRestClient = RxRestClient(baseUrl: URL(string:"\(baseUrl)/\(endpointVersion)"))
    
    func get<T : ResponseState>(_ endpoint : String, _ query : [String:Any]? = nil) -> Observable<T>  {
        if let query = query {
            return BaseService.client.get(endpoint, query: query)
        } else {
            return BaseService.client.get(endpoint)
        }
    }
}
