//
//  FollowerService.swift
//  football-list
//
//  Created by Benjamin on 08/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation

import RxSwift

class FollowerService : BaseService {
    let endpointFollowers = "followers"
    
    // returns an observable for followers for the given user
    func followersForUser(userId : String, slug : String? = nil) -> Observable<[User]> {
        let endpoint : String = "users/\(userId)/\(endpointFollowers)"
        let query : [String:Any]? = slug != nil ? ["current_follow_slug" : slug!] : nil
        let observable : Observable<FollowersResponseState> = get(endpoint,query)
        
        return observable.map({ (state) -> [User] in
            return (state.data?.followers)!
        })
    }
}
