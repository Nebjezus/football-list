//
//  User.swift
//  football-list
//
//  Created by Benjamin on 08/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation

struct User : Codable {
    let slug : String
    let firstname : String
    let lastname : String
    let profilePicture : String?
    let club : Club
    let primaryPosition : Position?
    let team : Team?
    
//    let facebookId : String? = nil
//    let roleId : Int = 1 // enum
//    let primaryPositionId : Int = 1
//    let countryId : Int = 1 // enum
//    let gender : String? = nil // enum
//    let email : String = ""
//    let googleId : String = ""
//    let digitsId : String? = nil
//    let phoneNumber : String?
//    let topPlayer : Bool = false
//    let createdAt : Date = Date.init()
//    let accountKitId : String? = nil
//    let isCoach : Bool = false
//    let coachTeamSlugs : [String] = [String]()
//    let role : String = "player" // enum
//    let isVerified : Bool = false
//    let anonymous : Bool = false
//    let isFollowing : Bool = falsevv
    
    enum CodingKeys: String, CodingKey {
        case slug
        case firstname
        case lastname
        case club
        case profilePicture = "profile_picture"
        case primaryPosition = "primary_position"
        case team
    }
 
    var fullname : String { return "\(firstname) \(lastname)" }
}
