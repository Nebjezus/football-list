//
//  Position.swift
//  football-list
//
//  Created by Benjamin on 08/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation

struct Position : Codable {
    let id : Int?
    let abbreviation : String?
    let name : String?
    let order : Int?
    let group : Int?
    let positionGroupId : Int?
    let localizedName : String?
    let localizedAbbreviation : String?

    enum CodingKeys: String, CodingKey {
        case id
        case abbreviation
        case name
        case order
        case group
        case positionGroupId = "position_group_id"
        case localizedName = "localized_name"
        case localizedAbbreviation = "localized_abbreviation"
    }
}
