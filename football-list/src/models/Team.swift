//
//  Team.swift
//  football-list
//
//  Created by Benjamin on 08/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation

struct Team : Codable {
    let id : Int
    let clubName : String?
    let displayName : String
    let leagueName : String
    let leagueSlug : String
    let name : String
    let requirePermissionToJoin : Bool
    let season : Season?
    let slug : String
    let playersCount : Int
    let numberOfCoaches : Int
//    let league : League
//    let club : Club
    
    enum CodingKeys: String, CodingKey {
        case id
        case clubName
        case displayName = "display_name"
        case leagueName = "league_name"
        case leagueSlug = "league_slug"
        case name
        case requirePermissionToJoin = "require_permission_to_join"
        case slug
        case season
        case playersCount = "players_count"
        case numberOfCoaches = "number_of_coaches"
    }
}
