//
//  Season.swift
//  football-list
//
//  Created by Benjamin on 08/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation

struct Season : Codable {
    let start : String
    let end : String
}
