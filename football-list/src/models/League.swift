//
//  League.swift
//  football-list
//
//  Created by Benjamin on 08/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation

struct League : Codable {
    let name : String
    let slug : String
    let id : Int
}
