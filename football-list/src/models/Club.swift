//
//  Club.swift
//  football-list
//
//  Created by Benjamin on 08/02/2019.
//  Copyright © 2019 Benjamin. All rights reserved.
//

import Foundation

struct Club : Codable {
    let slug : String?
    let name : String?
    let logoUrl : String?
    
    enum CodingKeys: String, CodingKey {
        case slug
        case name
        case logoUrl = "logo_url"
    }
}
